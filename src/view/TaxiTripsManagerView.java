package view;

import java.text.ParseException;
import java.util.Scanner;

import controller.Controller;
import model.vo.Taxi;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.Service;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) throws ParseException 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					
					
					// print menu cargar 
					
					printMenuCargar();
					
					//opcion cargar
					int optionCargar = sc.nextInt();
					
					//directorio json
					String linkJson = "";
					switch (optionCargar)
					{
					//direccion json pequeno
					case 1:

						linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
						
						break;

						//direccion json mediano
					case 2:

						linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
						break;

						//direccion json grande
					case 3:

						linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
						break;
					}
					
					//fecha inicial
					System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01T19:00:00.000)");
					String fechaInicial = sc.next();

					//fecha final 
					System.out.println("Ingrese la fecha final (Ej: 2017-02-01T19:00:00.000)");
					String fechaFinal = sc.next();
					
					//Cargar data
					 Controller.cargarSistema(linkJson, fechaInicial, fechaFinal);

					
		
					break;
					
			
				case 2:
					
					System.out.println("La lista de taxis organizados es:  ");	
					//Inicializar el arreglo con m�todo cargar.
					Taxi taxis[] = new Taxi[100000];
					Controller.ordenarHeap(taxis);
					break;
				case 3:
					//Inicializar arreglo con m�tod extra.
					Compania companias[] = Controller.arregloServicio();
					Controller.prioridadCompanias(companias);
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cargar");
		System.out.println("2.Ordenar taxis");
		System.out.println("3. Cola de prioridad");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	
	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	    
		
	}
}
