package controller;

import java.text.ParseException;

import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Compania;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId 
	 * @throws ParseException */
	public static void cargarSistema(String arch, String fechaI, String fechaF ) throws ParseException {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		boolean x = manager.cargarSistema( arch, fechaI, fechaF );
	}
		

	public static void ordenarHeap(Taxi taxis[])
	{
		manager.ordenarHeap(taxis);
	}
	public static void prioridadCompanias(Compania companias[])
	{
		manager.prioridadCompanias(companias);
	}
	
	public static Compania[] arregloServicio(){
		return  manager.arregloCompanias();
	}
}
