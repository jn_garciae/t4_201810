package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Service object
 */



public class Service implements Comparable<Service> {

	private String id, taxiId;
	
	private double miles;

	Double total;
	
	public Service(String id2, int id3, String Miles, String Total){
		
		id = id2;
		taxiId = id3+"";
		miles = Double.parseDouble(Miles);
		total = Double.parseDouble(Total);
		
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTime() {
		// TODO Auto-generated method stub
		return "trip start time";
	}

	
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public Date convertirDate(String fecha) {
		
		SimpleDateFormat fecha1 = new SimpleDateFormat("yyyy - dd -MM T HH:mm:ss");
	    Date retornar = null;
		try {
			retornar = fecha1.parse(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return retornar;
	
	}
}
