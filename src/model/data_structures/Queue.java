package model.data_structures;

import java.util.NoSuchElementException;

public class Queue<E> implements IQueue<E>{

	Node<E> first;

	Node<E> last;
	
	int longitud;
	
	
	@Override
	public void enqueue(E item) {
		Node<E> oldlast = last;
		last = new Node<E>(item);
		last.next = null;
		if (isEmpty()) first = last;
		else           oldlast.next = last;
		longitud++;

	}

	@Override
	public E dequeue() {
		if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        E item = first.item;
        first = first.next;
        longitud--;
        if (isEmpty()) last = null;   
        return item;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first==null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return longitud;
	}

	@Override
	public Iterator<E> iterator() {
		ListIterator<E> listIterator = new ListIterator<E>(first);
		return listIterator;
	}

	@Override
	public Queue<E> merge(int[] arr, int l, int m, int r) {
		// TODO Auto-generated method stub
		return null;
	}

}
