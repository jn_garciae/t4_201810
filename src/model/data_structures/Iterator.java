package model.data_structures;

public interface Iterator<Item>{

	public boolean hasNext();

	public Item Next();

	

}
