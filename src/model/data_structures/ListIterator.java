package model.data_structures;

import java.util.NoSuchElementException;

public class ListIterator<Item> implements Iterator<Item>{




	private Node<Item> proximo;
	private Item item;


	public ListIterator(Node<Item> r){

		proximo = r;

		item = proximo.item;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return proximo!=null ;
	}

	@Override
	public Item Next() {
		// TODO Auto-generated method stub
		if (!hasNext()){
			throw new NoSuchElementException("No hay proximo");
		}


		Item item2 = item;
		proximo = proximo.next; 
		return item2;

	}



}

