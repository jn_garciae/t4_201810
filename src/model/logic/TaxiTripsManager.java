package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.data_structures.Iterator;
import model.data_structures.Queue;
import model.vo.Compania;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
    private Queue<Compania> companias;
    
	public boolean cargarSistema(String arch, String fechaI, String fechaF) throws ParseException {
		// TODO Auto-generated method stub
		boolean c = false;
		JsonParser jp = new JsonParser();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Date in = formato.parse(fechaI);
		Date fnl = formato.parse(fechaF);
		try
		{
			JsonArray ja = (JsonArray) jp.parse(new FileReader(arch));
			for (int i = 0; ja !=null && i < ja.size(); i++)	
			{
				JsonObject jo = (JsonObject) ja.get(i);
				
				String inicial = jo.get("trip_end_timestamp").getAsString();
				Date fechaInicial = formato.parse(inicial);
				
				String fin = jo.get("trip_start_timestamp").getAsString();
				Date fechaFinal = formato.parse(fin);
				
				
				if(fechaInicial.after(in)&&fechaFinal.before(fnl)){
					
					
					String[] datosServicios = new String[2];
					String nombre = "NaN";
					if ( jo.get("company") != null )
					{ nombre = jo.get("company").getAsString();}
					Compania compania = new Compania(nombre);
					if(!existeCompa�ia(nombre))companias.enqueue(compania);
					
					
					String taxiId = "";
					if ( jo.get("taxi_id") != null )
					{ taxiId = jo.get("taxi_id").getAsString(); }
					
					
					if ( jo.get("trip_id") != null )datosServicios[0] =jo.get("tripId").getAsString();
					
					
					if ( jo.get("trip_miles") != null )datosServicios[1] =jo.get("trip_miles").getAsString();
					
					
					if ( jo.get("trip_total") != null )datosServicios[2] =jo.get("trip_total").getAsString();
					
					compania.addTaxi(taxiId,datosServicios);
					
					
					}
					  
					  
					  
				
				System.out.println("------------------");
				System.out.println(jo);
				if(jo != null)
				{
					c = true;
				}
				
				}
				
				
			
			JsonReader jr = new JsonReader(new FileReader(arch));
			Gson gs = new GsonBuilder().create();

			jr.beginArray();	
			jr.close();
				}
			
		catch(IOException e)
		{
			e.printStackTrace();

		}
	
		return c;
	}
		
		
	public boolean existeCompa�ia(String name){
		
		Iterator<Compania> iterador = companias.iterator();
		boolean encontro = false;
		while(iterador.hasNext()&&!encontro){
			Compania actual = iterador.Next();
			
			if(actual.name().equals(name))encontro = true;
			}
		return encontro; 
	}
	public void ordenarHeap(Taxi taxis[])
	{
		int a = taxis.length;
		for(int i = a/2 - 1; i >= 0; i--)
		{
			taxisHeap(taxis, a, i);
		}
		for(int i = a-1; i>=0; i--)
		{
			Taxi t = taxis[0];
			taxis[0] = taxis[i];
			taxis[i] = t;
			taxisHeap(taxis, i, 0);
		}
		
	}
	public void taxisHeap(Taxi taxis[], int a, int i)
	{
		int menor = i;
		int izq = 2*i + 1;
		int d = 2*1 + 2;
		if( izq < a && taxis[izq].compareTo(taxis[menor]) == 0)
		{
			menor = izq;
		}
		if(d < a && taxis[d].compareTo(taxis[menor])== 0)
		{
			menor = d;
		}
		if(menor != i)
		{
			Taxi c = taxis[i];
			taxis[i] = taxis[menor]; 
			taxis[menor] = c;
			taxisHeap(taxis, a, menor);
		}
	}
	public void prioridadCompanias( Compania companias[])
	{
		int a = companias.length;
		for(int i = a/2 - 1; i >= 0; i--)
		{
			companiasHeap(companias, a, i);
		}
		for(int i = a-1; i>=0; i--)
		{
			Compania c=  companias[0];
			companias[0] = companias[i];
			companias[i] = c;
			companiasHeap(companias, i, 0);
		}
	}
	public void companiasHeap(Compania companias[], int a, int i)
	{
		int mayor = i;
		int izq = 2*i + 1;
		int d = 2*1 + 2;
		if( izq < a && companias[izq].compareTo(companias[mayor]) != 0)
		{
			mayor = izq;
		}
		if(d < a && companias[d].compareTo(companias[mayor])!= 0)
		{
			mayor = d;
		}
		if(mayor != i)
		{
			Compania c = companias[i];
			companias[i] = companias[mayor]; 
			companias[mayor] = c;
			companiasHeap(companias, a, mayor);
		}
	}

	public Compania[] arregloCompanias(){
		Iterator<Compania> iterador = companias.iterator();
		int i = 0;
		Compania[] companias1 = new Compania[companias.size()];
		boolean encontro = false;
		while(iterador.hasNext()){
		Compania actual = iterador.Next();
			companias1[i]= actual;
			i++;
	}
   return companias1;
}
	
	
}
